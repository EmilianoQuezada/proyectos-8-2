import express from 'express'
import alumnosDb from '../models/alumnos.js';

export const router = express.Router();

// declarar primer ruta por omision
router.get('/',(req,res)=>{
    res.render('index',{titulo:"Mis Practicas Js", nombre:"Quezada Ramos Julio Emiliano"})

});

router.get('/tabla', (req,res)=>{
    //Parametros
    const parameto = {
        numero:req.query.numero
    }
    res.render('tabla', parameto);
})

router.post('/tabla', (req,res)=>{
    //Parametros
    const parameto = {
        numero:req.body.numero
    }
    res.render('tabla', parameto);
})

router.get('/cotizaciones', (req,res)=>{
    //Parametros
    const parameto = {
        folio: req.query.folio,
        descripcion:req.query.descripcion,
        valor: req.query.valor,
        pInicial: req.query.pInicial,
        plazos: req.query.plazos
    }
    res.render('cotizaciones', parameto);
})

router.post('/cotizaciones', (req,res)=>{
    //Parametros
    const parameto = {
        folio: req.body.folio,
        descripcion:req.body.descripcion,
        valor: req.body.valor,
        pInicial: req.body.pInicial,
        plazos: req.body.plazos
    }
    res.render('cotizaciones', parameto);
})

let params;
router.post('/alumnos',async(req,res)=>{
    try{
     params={
        matricula:req.body.matricula,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        sexo:req.body.sexo,
        especialidad:req.body.especialidad   
     }
    const res = await alumnosDb.insertar(params);

    } catch(error){
        console.error(error)
        res.status(400).send("sucedio un error:" + error);
    }
    res.redirect('/alumnos');
});

let rows;

router.get('/alumnos',async(reg,res)=>{
    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos',{reg:rows});
    
})


export default {router}